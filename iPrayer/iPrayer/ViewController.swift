//
//  ViewController.swift
//  iPrayer
//
//  Created by Moktadirul Islam on 1/16/19.
//  Copyright © 2019 moktadir. All rights reserved.
//

import UIKit
import CoreLocation
import Adhan

class ViewController: UIViewController,CLLocationManagerDelegate {

    var locationManager: CLLocationManager!
    var isPrayerCalled: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        determineMyCurrentLocation()
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        locationManager.stopUpdatingLocation()
        
        print("User Location latitude = \(userLocation.coordinate.latitude)")
        print("User Location longitude = \(userLocation.coordinate.longitude)")
        
        if !isPrayerCalled {
            isPrayerCalled = true
            getPrayerTime(userLocation: userLocation)
        }
    }
    
    func getPrayerTime(userLocation:CLLocation) {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let date = calendar.dateComponents([.year, .month, .day], from: Date())
        let coordinate = Coordinates(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        var params = CalculationMethod.muslimWorldLeague.params
        params.madhab = .hanafi
        if let prayers = PrayerTimes(coordinates: coordinate, date: date, calculationParameters: params) {
            let formatter = DateFormatter()
            formatter.timeStyle = .medium
            formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)!
            
            print("fajr \(formatter.string(from: prayers.fajr))")
            print("sunrise \(formatter.string(from: prayers.sunrise))")
            print("dhuhr \(formatter.string(from: prayers.dhuhr))")
            print("asr \(formatter.string(from: prayers.asr))")
            print("maghrib \(formatter.string(from: prayers.maghrib))")
            print("isha \(formatter.string(from: prayers.isha))")
        }
        
        let prayerTimes = PrayerTimes(coordinates: coordinate, date: date, calculationParameters: params)
        
        let current = prayerTimes?.currentPrayer()
        let next = prayerTimes?.nextPrayer()
        let countdown = prayerTimes?.time(for: next!)
        
        print("Current:\(current)  NExt:\(next)  CountDown :: \(countdown)")
    }
    




}

