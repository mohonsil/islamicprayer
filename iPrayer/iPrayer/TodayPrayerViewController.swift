//
//  TodayPrayerViewController.swift
//  iPrayer
//
//  Created by Moktadirul Islam on 4/1/19.
//  Copyright © 2019 moktadir. All rights reserved.
//

import UIKit
import CoreLocation
import Adhan

enum NextPreyer : String {
    case fajr = "Fajr"
    case sunrise = "Sunrise"
    case dhuhr = "Dhuhr"
    case asr = "Asr"
    case maghrib = "Maghrib"
    case isha = "Isha"
}

class TodayPrayerViewController: UIViewController,CLLocationManagerDelegate {

    var locationManager: CLLocationManager!
    var isPrayerCalled: Bool = false
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var nextPrayerName: UILabel!
    
    @IBOutlet weak var nextPrayerTimeLabel: UILabel!
    @IBOutlet weak var remainingTime: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let settingImage = UIImage(named: "IconSettings")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: settingImage, style: .plain, target: self, action: #selector(addTapped))

        setupTitleView()
        determineMyCurrentLocation()
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        locationManager.stopUpdatingLocation()
        
        print("User Location latitude = \(userLocation.coordinate.latitude)")
        print("User Location longitude = \(userLocation.coordinate.longitude)")
        
        CLGeocoder().reverseGeocodeLocation(userLocation, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                return
            }else if let country = placemarks?.first?.country,
                let city = placemarks?.first?.locality {
                print(country)
                self.cityNameLabel.text = city
            }
            else {
            }
        })
        
        
        if !isPrayerCalled {
            isPrayerCalled = true
            getPrayerTime(userLocation: userLocation)
        }
    }
    
    func getPrayerTime(userLocation:CLLocation) {
        var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: TimeZone.current.identifier)!
        let date = calendar.dateComponents([.year, .month, .day], from: Date())
        let coordinate = Coordinates(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        var params = CalculationMethod.muslimWorldLeague.params
        params.madhab = .hanafi
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "h:mm a"
        formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)!
        //let currentDate = formatter.date(from: date)
        if let prayers = PrayerTimes(coordinates: coordinate, date: date, calculationParameters: params) {

            print("fajr \(formatter.string(from: prayers.fajr))")
            print("sunrise \(formatter.string(from: prayers.sunrise))")
            print("dhuhr \(formatter.string(from: prayers.dhuhr))")
            print("asr \(formatter.string(from: prayers.asr))")
            print("maghrib \(formatter.string(from: prayers.maghrib))")
            print("isha \(formatter.string(from: prayers.isha))")
        }
        
        let prayerTimes = PrayerTimes(coordinates: coordinate, date: date, calculationParameters: params)
        if let next = prayerTimes?.nextPrayer(){
            nextPrayerName.text = nextPrayerNameFromPrayer(for: next).rawValue
            let countdown = prayerTimes?.time(for: next)
            nextPrayerTimeLabel.text = formatter.string(from: countdown!)
            
//            let remainingTimee:String? = Date.offsetFrom(countdown!)
//            remainingTime.text = remainingTimee
        }
    }
    
    public func nextPrayerNameFromPrayer(for prayer: Prayer) -> NextPreyer {
        switch prayer {
        case .fajr:
            return NextPreyer.fajr
        case .sunrise:
            return NextPreyer.sunrise
        case .dhuhr:
            return NextPreyer.dhuhr
        case .asr:
            return NextPreyer.asr
        case .maghrib:
            return NextPreyer.maghrib
        case .isha:
            return NextPreyer.isha
        }
    }
    
    private func setupTitleView() {
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd"
        let nameOfMonth = dateFormatter.string(from: now)
        
        let hijri = NSCalendar(identifier: NSCalendar.Identifier.islamicUmmAlQura)
        let dateFormatterIslamic = DateFormatter()
        dateFormatterIslamic.dateFormat = "MMMM dd, yyyy"
        dateFormatterIslamic.calendar = hijri as Calendar?
        let dateString = dateFormatterIslamic.string(from: now)

        let topText = nameOfMonth
        let bottomText = "\(dateString) AH"
        
        let titleParameters = [NSAttributedString.Key.foregroundColor : UIColor.black,
                               NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
        let subtitleParameters = [NSAttributedString.Key.foregroundColor : UIColor.gray,
                                  NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13)]
        
        let title:NSMutableAttributedString = NSMutableAttributedString(string: topText, attributes: titleParameters)
        let subtitle:NSAttributedString = NSAttributedString(string: bottomText, attributes: subtitleParameters)
        
        title.append(NSAttributedString(string: "\n"))
        title.append(subtitle)
        
        let size = title.size()
        
        let width = size.width
        guard let height = navigationController?.navigationBar.frame.size.height else {return}
        
        let titleLabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: width, height: height))
        titleLabel.attributedText = title
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        
        navigationItem.titleView = titleLabel
    }
    
    @objc func addTapped() {
        print("Add Tapped")
    }
    
}

extension Date {
    
    func offsetFrom(date : Date) -> String? {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self);
        
        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    
}
