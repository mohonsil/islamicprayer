//
//  PrayersViewController.swift
//  iPrayer
//
//  Created by Moktadirul Islam on 4/1/19.
//  Copyright © 2019 moktadir. All rights reserved.
//

import UIKit

import CoreLocation
import Adhan

class PrayersViewController: UIViewController,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateEnglishLabel: UILabel!
    @IBOutlet weak var dateArabicLabel: UILabel!
    
    var  prayer:PrayerTimes?
    
    let cellReuseIdentifier = "PrayerTimeCell"
    
    var locationManager: CLLocationManager!
    var isPrayerCalled: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        determineMyCurrentLocation()
        setUpUI()
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func setUpUI()  {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd"
        let nameOfMonth = dateFormatter.string(from: now)
        
        let hijri = NSCalendar(identifier: NSCalendar.Identifier.islamicUmmAlQura)
        let dateFormatterIslamic = DateFormatter()
        dateFormatterIslamic.dateFormat = "MMMM dd, yyyy"
        dateFormatterIslamic.calendar = hijri as Calendar?
        let dateString = dateFormatterIslamic.string(from: now)
        
        dateEnglishLabel.text = nameOfMonth
        dateArabicLabel.text = dateString
    }
    
    @objc func clickOnButton() {
        print("ITem clicked")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        locationManager.stopUpdatingLocation()
        
        print("User Location latitude = \(userLocation.coordinate.latitude)")
        print("User Location longitude = \(userLocation.coordinate.longitude)")
        
        CLGeocoder().reverseGeocodeLocation(userLocation, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                return
            }else if let country = placemarks?.first?.country,
                let city = placemarks?.first?.locality {
                print(country)
                self.UpdateNavigationTitle(title: "\(city), \(country)")
            }
            else {
            }
        })
        
        
        if !isPrayerCalled {
            isPrayerCalled = true
            getPrayerTime(userLocation: userLocation)
        }
    }
    
    func UpdateNavigationTitle(title:String) {
        let topText = title
        let bottomText = "Muslim World League(18°/17°)"
        
        let titleParameters = [NSAttributedString.Key.foregroundColor : UIColor.black,
                               NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
        let subtitleParameters = [NSAttributedString.Key.foregroundColor : UIColor.gray,
                                  NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13)]
        
        let title:NSMutableAttributedString = NSMutableAttributedString(string: topText, attributes: titleParameters)
        let subtitle:NSAttributedString = NSAttributedString(string: bottomText, attributes: subtitleParameters)
        
        title.append(NSAttributedString(string: "\n"))
        title.append(subtitle)
        
        let size = title.size()
        
        let width = size.width
        guard let height = navigationController?.navigationBar.frame.size.height else {return}
        
        let titleButton = UIButton(type: .custom)
        titleButton.frame = CGRect(x: 0.0, y: 0.0, width: width, height: height)
        titleButton.setAttributedTitle(title, for: .normal)
        titleButton.titleLabel?.numberOfLines = 0
        titleButton.titleLabel?.lineBreakMode = .byWordWrapping
        titleButton.titleLabel?.textAlignment = .center
        titleButton.addTarget(self, action: #selector(clickOnButton), for: .touchUpInside)
        navigationItem.titleView = titleButton

    }
    
    func getPrayerTime(userLocation:CLLocation) {
        var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: TimeZone.current.identifier)!
        let date = calendar.dateComponents([.year, .month, .day], from: Date())
        let coordinate = Coordinates(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        var params = CalculationMethod.muslimWorldLeague.params
        params.madhab = .hanafi
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)!
        //let currentDate = formatter.date(from: date)
        if let prayers = PrayerTimes(coordinates: coordinate, date: date, calculationParameters: params) {
            
            print("fajr \(formatter.string(from: prayers.fajr))")
            print("sunrise \(formatter.string(from: prayers.sunrise))")
            print("dhuhr \(formatter.string(from: prayers.dhuhr))")
            print("asr \(formatter.string(from: prayers.asr))")
            print("maghrib \(formatter.string(from: prayers.maghrib))")
            print("isha \(formatter.string(from: prayers.isha))")
            self.prayer = prayers
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if prayer != nil{
            return 6
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PrayerTimeTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! PrayerTimeTableViewCell
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "h:mm a"
        formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)!
        if indexPath.row == 0{
            cell.prayerNameLabel.text = "Fajr"
            cell.prayerTimeLabel.text = formatter.string(from: (prayer?.fajr)!)
        }else if indexPath.row == 1{
            cell.prayerNameLabel.text = "Sunrise"
            cell.prayerTimeLabel.text = formatter.string(from: (prayer?.sunrise)!)
        }else if indexPath.row == 2{
            cell.prayerNameLabel.text = "Dhuhr"
            cell.prayerTimeLabel.text = formatter.string(from: (prayer?.dhuhr)!)
        }else if indexPath.row == 3{
            cell.prayerNameLabel.text = "Asr"
            cell.prayerTimeLabel.text = formatter.string(from: (prayer?.asr)!)
        }else if indexPath.row == 4{
            cell.prayerNameLabel.text = "Maghrib"
            cell.prayerTimeLabel.text = formatter.string(from: (prayer?.maghrib)!)
        }else if indexPath.row == 5{
            cell.prayerNameLabel.text = "Isha"
            cell.prayerTimeLabel.text = formatter.string(from: (prayer?.isha)!)
        }
        
        return cell
    }

}
